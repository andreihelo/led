Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'led#menu'
  get  '/led/on',  to: 'led#on'
  get  '/led/off', to: 'led#off'
end
